package com.javispedro.autobluetether;

import android.bluetooth.BluetoothDevice;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a list of {@link BluetoothDevice},
 * checkboxes show whether the device is in "enabled set" or not
 */
public class BtDeviceRecyclerViewAdapter extends RecyclerView.Adapter<BtDeviceRecyclerViewAdapter.ViewHolder> {
    private final static String TAG = "BtDeviceRecyclerViewAd";

    private List<BluetoothDevice> mValues = new ArrayList<>();
    private BtDeviceEnabledSetAdapter mEnabledAdapter;

    public void setValues(@NonNull List<BluetoothDevice> values) {
        mValues = values;
        Log.d(TAG, "values size:" + values.size());
        notifyDataSetChanged();
    }

    public void setBtDeviceEnabledSetAdapter(@NonNull BtDeviceEnabledSetAdapter adapter) {
        mEnabledAdapter = adapter;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_device, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.setItem(mValues.get(position));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView mNameView;
        private final TextView mAddressView;
        private final CheckBox mCheckBox;
        private BluetoothDevice mItem;

        public ViewHolder(View view) {
            super(view);
            mNameView = view.findViewById(R.id.name);
            mAddressView = view.findViewById(R.id.address);
            mCheckBox = view.findViewById(R.id.checked);
            mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (mEnabledAdapter != null && mItem != null) {
                        Log.d(TAG, "onCheckedChanged " + mItem.getName() + " " + isChecked);
                        mEnabledAdapter.set(mItem, isChecked);
                    }
                }
            });
        }

        public void setItem(BluetoothDevice device) {
            mItem = device;
            mNameView.setText(device.getName());
            mAddressView.setText(device.getAddress());
            if (mEnabledAdapter != null) {
                mCheckBox.setChecked(mEnabledAdapter.contains(device));
            }
        }

        @NonNull
        @Override
        public String toString() {
            return super.toString() + " [" + mItem.getAddress() + "]";
        }
    }
}