package com.javispedro.autobluetether;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;

public class BtTetherEnabler extends ContextWrapper {
    private final static String TAG = "BtTetherEnabler";

    public BtTetherEnabler(Context context) {
        super(context);
    }

    public static class EnableException extends Exception {
        public EnableException(String message, Throwable cause) {
            super(message, cause);
        }

        public EnableException(String message) {
            super(message);
        }
    }

    public void startTethering() throws EnableException {
        final BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null) {
            Log.e(TAG, "no BT adapter");
            throw new EnableException("No Bluetooth adapter");
        }

        adapter.getProfileProxy(getBaseContext(), new BluetoothProfile.ServiceListener() {
            public void onServiceConnected(int profile, BluetoothProfile proxy) {
                Log.d(TAG, "onServiceConnected");
                final BtPanWrapper wrapper = new BtPanWrapper(proxy);
                boolean is_on = wrapper.isTetheringOn();
                Log.d(TAG, "isTetheringOn: " + is_on);
                if (!is_on) {
                    Log.d(TAG, "trying to turn on");
                    wrapper.setBluetoothTethering(true);
                }
                adapter.closeProfileProxy(profile, proxy);
            }

            public void onServiceDisconnected(int profile) {
                Log.d(TAG, "onServiceDisconnected");
            }
        }, BtPanWrapper.PROFILE_ID);
    }


}
