package com.javispedro.autobluetether;

import android.bluetooth.BluetoothProfile;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BtPanWrapper {
    private static final String CLASS_NAME = "android.bluetooth.BluetoothPan";
    public static final int PROFILE_ID = 5 /*BluetoothProfile.PAN*/;
    private final BluetoothProfile p;

    public BtPanWrapper(BluetoothProfile pan) {
        p = pan;
    }

    private Object invoke(String methodName, Class[] parameterTypes, Object[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, ClassNotFoundException {
        Class c = Class.forName(CLASS_NAME);
        Method m = c.getDeclaredMethod(methodName, parameterTypes);
        return m.invoke(this.p, args);
    }

    public boolean isTetheringOn() {
        try {
            return (boolean) invoke("isTetheringOn", null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setBluetoothTethering(boolean on) {
        try {
            invoke("setBluetoothTethering", new Class[] {Boolean.TYPE}, new Object[] {on});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
