package com.javispedro.autobluetether;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 */
public class DeviceFragment extends Fragment {
    private final static String TAG = "DeviceFragment";

    private RecyclerView mRecyclerView;
    private BtDeviceRecyclerViewAdapter mAdapter;
    private BtDeviceEnabledSetAdapter mEnabledAdapter;

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DeviceFragment() {
    }

    @SuppressWarnings("unused")
    public static DeviceFragment newInstance(int columnCount) {
        DeviceFragment fragment = new DeviceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_device_list, container, false);
        mRecyclerView = (RecyclerView) view;

        Context context = view.getContext();

        if (mColumnCount <= 1) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            mRecyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        mEnabledAdapter = new BtDeviceEnabledSetAdapter(prefs, getString(R.string.prefs_key_device_list));

        mAdapter = new BtDeviceRecyclerViewAdapter();
        mAdapter.setBtDeviceEnabledSetAdapter(mEnabledAdapter);
        mRecyclerView.setAdapter(mAdapter);

        return mRecyclerView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRecyclerView = null;
        mAdapter = null;
        mEnabledAdapter = null;
    }

    public void refreshList() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null) {
            Log.e(TAG, "no bluetooth adapter found?");
            Toast.makeText(getContext(), getString(R.string.no_bluetooth_adapter), Toast.LENGTH_LONG).show();
            return;
        }
        ArrayList<BluetoothDevice> devices = new ArrayList<>(adapter.getBondedDevices());
        mAdapter.setValues(devices);
    }
}