package com.javispedro.autobluetether;

import android.bluetooth.BluetoothDevice;
import android.content.SharedPreferences;
import android.util.ArraySet;
import android.util.Log;

import java.util.Set;

public class BtDeviceEnabledSetAdapter {
    private final static String TAG = "BtDeviceEnabledSetAd";

    private final SharedPreferences mPrefs;
    private final String mPrefsKey;
    private final ArraySet<String> mSet = new ArraySet<>();

    public BtDeviceEnabledSetAdapter(SharedPreferences prefs, String key) {
        mPrefs = prefs;
        mPrefsKey = key;

        readPrefs();
    }

    public boolean contains(BluetoothDevice device) {
        return mSet.contains(device.getAddress());
    }

    public void set(BluetoothDevice device, boolean enabled) {
        String address = device.getAddress();
        boolean currently = mSet.contains(address);
        if (enabled && !currently) {
            mSet.add(address);
            savePrefs();
        } else if (currently && !enabled) {
            mSet.remove(address);
            savePrefs();
        }
    }

    private void readPrefs() {
        mSet.clear();
        Set<String> prefsSet = mPrefs.getStringSet(mPrefsKey, null);
        if (prefsSet != null) {
            mSet.addAll(prefsSet);
        }
    }

    private void savePrefs() {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putStringSet(mPrefsKey, mSet);
        editor.apply();
        Log.d(TAG, "saved " + mPrefsKey + ":" + mSet);
    }
}
