package com.javispedro.autobluetether;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public class SystemSettingsFragment extends Fragment {
    public SystemSettingsFragment() {
        // Required empty public constructor
    }

    public static SystemSettingsFragment newInstance() {
        return new SystemSettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_system_settings, container, false);
    }
}