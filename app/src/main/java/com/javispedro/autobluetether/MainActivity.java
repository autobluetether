package com.javispedro.autobluetether;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "MainActivity";

    private SystemSettingsFragment systemSettingsFragment;
    private DeviceFragment deviceFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager manager = getSupportFragmentManager();
        systemSettingsFragment = (SystemSettingsFragment) manager.findFragmentById(R.id.fragmentSystemSettings);
        deviceFragment = (DeviceFragment) manager.findFragmentById(R.id.fragmentDeviceList);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshSystemSettingsFragmentVisible();
        refreshDeviceList();
    }

    @Override
    protected void onDestroy() {
        deviceFragment = null;
        systemSettingsFragment = null;
        super.onDestroy();
    }

    private void refreshSystemSettingsFragmentVisible() {
        boolean can_write_settings = Settings.System.canWrite(this);
        Log.d(TAG, "can_write_settings: " + can_write_settings);
        setSystemSettingsFragmentVisible(!can_write_settings);
    }

    private void refreshDeviceList() {
        deviceFragment.refreshList();
    }

    private void setSystemSettingsFragmentVisible(boolean visible) {
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        if (visible) {
            tx.show(systemSettingsFragment);
        } else {
            tx.hide(systemSettingsFragment);
        }
        tx.commit();
    }

    public void manageWriteSettings(View view) {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
    }
}